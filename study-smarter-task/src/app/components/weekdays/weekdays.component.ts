import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-weekdays',
  templateUrl: './weekdays.component.html',
  styleUrls: ['./weekdays.component.scss']
})
export class WeekdaysComponent implements OnInit {

  @Input() day: string;

  monIsActive = false;
  tueIsActive = false;
  wedIsActive = false;
  thuIsActive = false;
  friIsActive = false;
  satIsActive = false;
  sunIsActive = false;


  constructor() { }

  ngOnInit(): void {
    this.setActiveDay(this.day);
  }

  setActiveDay(day: string ){

    switch (day) {
      case 'mon':
        this.monIsActive = true;
        break;
      case 'tue':
        this.tueIsActive = true;
        break;
      case 'wed':
        this.wedIsActive = true;
        break;
      case 'thu':
        this.thuIsActive = true;
        break;
      case 'fri':
        this.friIsActive = true;
        break;
      case 'sat':
        this.satIsActive = true;
        break;
      case 'sun':
        this.sunIsActive = true;
        break;
      default: return;
    }
  }
}
