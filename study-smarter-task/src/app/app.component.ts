import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent  implements OnInit {
  title = 'study-smarter-task';
  currentDate: string;

  ngOnInit(): void {
    this.currentDate = new Date().toLocaleString('en', { weekday: 'short' }).toString().toLowerCase();
  }
}
